# 💚 Masnavi

Apresentamos a versão WEB, reformatada e revisada da tradução de _Sayed Omar Naqshband_ do **"Masnavi de Maulana Jalaluddin Rum".**&#x20;

O objetivo desta nova versão é tornar o Masnavi mais acessível a todos de forma democrática e prática, não há interesse financeiro nesta públicação e respeitamos todos os direitos biográficos da obra.

O Masnavi é uma públicação livre de direitos autorais, mas sua impressão envolve custos, a tradução atual também é um trabalho realizado por _Sayed Omar Naqshband_, e envolveu seus custos e não queremos de forma alguma desvalorizar tal trabalho.

Esta versão Web foi revisada também com base na versão em ingês "**The Masnavi I Ma'navi of Rumi Complete**" By _"Maulana 'alalu·'d-din Muhammad Rumi"_, traduzida do Urdu por "_E.H. Whinfield_"


#### Traduções já realizadas:

* Português
* 

## Formatos Disponíveis

* [PDF](./book.pdf)
