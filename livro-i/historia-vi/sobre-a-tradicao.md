# Sobre A Tradição

### "Eu era um tesouro oculto e desejva ser conheco, e criei o mundo para que eu fosse conhecido"

Destrói tua casa e com o tesouro nela escondido (225)

Serás capaz de construir milhares de casas.

O tesouro jaz sob ela; não há remédio para ela; 

Não hesites em pô-la abaixo; não demores! 

Pois com a moeda desse tesouro 

Mil casas podem ser construídas sem trabalho. 

Por fim, com certeza essa casa será destruída, 

E o tesouro divino será visto debaixo dela.

Mas ele então não pertencerá a ti, pois em verdade 

Esse prêmio é o salário por destruir a casa. 

Quando não se trabalha, não se recebe salário; 

"O homem não recebe nada pelo que não tenha trabalhado".(226) 

Então morderás teu dedo, dizendo: 

&#x20;  "Ai! Essa lua brilhante estava escondida atrás de uma nuvem. 

&#x20;   Não fiz o que para o meu bem disseram; 

&#x20;   Agora a casa e o tesouro estão perdidos, e minha mão está vazia". 

Arrendaste tua casa ou a alugaste; 

Ela não é tua propriedade para comprar e vender. 

Quanto ao prazo de locação, é até tua morte; 

Nesse prazo, terás de fazer com ela algo de útil. 

Se antes do término da locação

Deixares de tirar proveito da casa, 

Então o dono te expulsará 

E a derrubará ele mesmo para encontrar a mina de ouro, Enquanto tu estarás ora golpeando tua cabeça em profundo arrependimento, 

Ora arrancando tua barba ao pensar em tua tolice, 

Dizendo: 

&#x20;  "Ai! Essa casa me pertencia; 

&#x20;   Fui cego e não tirei proveito dela. 

&#x20;   Ai! O vento carregou minha morada 

&#x20;   Para sempre! 'Ó desgraça que recai sobre escravos!' (227)

&#x20;   Nessa minha casa só vi formas e quadros; 

&#x20;   Estava encantado com essa casa tão efêmera! 

&#x20;   Ignorava o tesouro escondido embaixo dela, 

&#x20;   Senão teria agarrado um machado como um perfume. 

&#x20;   Ah! Se eu tivesse aplicado a justiça do machado, 

&#x20;   Estaria agora livre da dor. 

&#x20;   Mas fexei meu olhar nas formas exteriores; 

&#x20;   Como uma criança, diverti-me com brinquedos. 

&#x20;   Bem disse o famoso Hakim Sanai: 

&#x20;       'Tu és uma criança; tua casa está cheia de quadros!' 

&#x20;  Em seu divino poema, ele dá este conselho:

&#x20;       "Varre a poeira de tua casa!'“

----

225- Comparar com o hadith: ''Morre antes de morrer", isto é, mortifica teus desejos carnais, e encontrarás o tesouro espiritual.

226- [Alcorão III.39](https://quran.com/3/39)
