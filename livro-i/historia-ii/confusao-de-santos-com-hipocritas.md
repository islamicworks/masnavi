# Confusão de Santos Com Hipócritas

Os sentidos mundanos são a escada da terra,

Os sentidos espirituais são a escada do céu.

A saúde daqueles busca-se junto ao médico,

A saúde destes, junto ao Amigo.

A saúde daqueles se obtém cuidando do corpo,

A destes, mortificando a carne.

A alma nobre arruina o corpo

E, depois de sua destruição, o constrói de novo.

Feliz a alma que por amor a Deus

Abandonou família, riqueza e bens!

Destruiu sua casa para encontrar o tesouro escondido,

E com esse tesouro reconstruiu-a mais bela;

Represou as águas e limpou o canal,

Depois desviou novas águas para dentro do canal;

Cortou sua carne para extrair a ponta de uma lança,

Fazendo uma nova pele crescer sobre a ferida;

Arrasou a fortaleza para expulsar o infiel que a guardava,

E depois reconstruiu-a com cem torres e baluartes.

Quem pode descrever o trabalho singular da Graça?

Fui forçado a ilustrá-lo por estas metáforas

Ora sob uma aparência, ora sob outra.

Sim, as coisas da religião são só perplexidade;

Não como ocorre quando se dá as costas a Deus,

E sim como afogar-se e ser absorvido n'Ele.

O que assim o faz tem o rosto sempre voltado a Deus,

Enquanto o do outro mostra sua indisciplinada obstinação.

Observa o rosto de cada um, olha-os bem.

Pode ser que, como servo, reconheças o rosto da Verdade.

Já que existem muitos demônios com rosto de homens,

É um erro dar a mão a todo mundo.

Quando o passarinheiro faz soar seu pio traiçoeiro,

Para com esse ardil seduzir os pássaros,

Eles ouvem esse chamado, que seu próprio canto simula

E, baixando à terra, encontram rede e punhal.

Assim os vis hipócritas roubam a linguagem dos dervixes,

Para enganar a gente simples com seus truques.

As obras dos justos são luz e calor,

As obras dos maus, traição e impudor.

Fazem leões empalhados para assustar os simples,

Dão título de Mohammed ao falso Musailima.

Mas Musailima guardou o nome de "Mentiroso",

E Mohammed o de "Mais sublime dos seres".

O vinho de Deus exala perfume de almíscar,

Outro vinho está reservado para punição e penas.
