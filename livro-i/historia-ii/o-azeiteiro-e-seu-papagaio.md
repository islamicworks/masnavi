# O Zeiteiro e Seu Papagaio

Um azeiteiro tinha um papagaio que costumava diverti-lo com sua tagarelice e que vigiava a loja quando ele saía. Certo dia, quando o papagaio estava sozinho na loja, um gato derrubou uma das talhas de azeite. Quando o azeiteiro voltou, pensou que o papagaio é que tivesse feito o estrago e, em sua fúria, desferiu tal golpe na cabeça do papagaio que todas as suas penas caíram, deixando-o tão atordoado que perdeu a fala por vários dias.

Um dia, porém, o papagaio viu passar pela loja um homem careca e, recobrando a fala, gritou: "Por favor, de quem era a talha de azeite que derrubaste?" Os passantes riram do engano do papagaio, que confundira a calvície provocada pela idade com a perda de suas próprias penas devido a um safanão.



