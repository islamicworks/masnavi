# Descrição do Amor

O amante se prova verdadeiro pela dor em seu coração; 

Não há mal como o mal do coração. 

A dor do amante é diferente de todas as dores; 

O amor é o astrolábio dos mistérios de Deus.

Pode o amante suspirar por este ou aquele amor, 

Mas no fim é atraído ao Rei do amor. 

Por mais que se descreva ou se explique o amor, 

Quando nos apaixonamos envergonhamo-nos de nossas palavras.

A explicação pela língua esclarece a maioria das coisas, 

Mas o amor não explicado é mais claro. 

Quando a pena se apressou em escrever, 

Ao chegar no tema do amor, partiu-se em duas. 

Quando o discurso tocou na questão do amor, 

A pena partiu-se e o papel rasgou-se. 

Ao explicá-lo, a razão logo empaca, como um asno no atoleiro;

Nada senão o próprio Amor pode explicar o amor e os amantes!

Ninguém senão o Sol pode revelar o sol, 

Se o vires revelado, não lhe dês as costas. 

Sombras, de fato, podem indicar a presença do sol, 

Mas só o Sol revela a luz da vida. 

Sombras trazem sonolência, como as conversas ao anoitecer, 

Mas quando o sol se levanta, "a lua está fendida". (6) 

No mundo, nada é tão prodigioso como o sol, 

Mas o Sol da alma não se põe e não possui ontem. 

Embora o sol material seja único e singular, 

Podemos conceber sóis semelhantes a ele. 

Mas ao Sol da alma, além deste firmamento,

Nada se iguala, seja concreto ou abstrato. 

Onde haverá lugar na concepção para 

Sua essência, Para que algo similar a Ele seja concebível?

----

6- [Alcorão LIV.l.](https://quran.com/54/1)
