---
description: Este conto/poema não consta na versão inglêsa, não nessa ordem.
---

# Shamsudin De Tabriz Assedia Jalaluddin Para Que Componha o Masnavi
(7)

O sol (Shams) de Tabriz é uma luz perfeita, 

Sol, sim, um dos raios de Deus! 

Quando se ouviu o louvor do "Sol de Tabriz", 

O sol do quarto céu baixou a cabeça. 

Agora que mencionei seu nome, é justo expor

Alguns sinais de sua beneficência. 

Essa Alma preciosa segurou na borda de meu manto, 

Exalando o perfume da roupa de Yussuf (José); 

E disse: 

&#x20;  "Por amor a nossa antiga amizade, 

&#x20;   Fala um pouco daqueles doces estados de êxtase 

&#x20;   Para que a terra e o céu possam alegrar-se, 

&#x20;   E também a Razão e o Espírito, cem vezes".

Eu disse: 

&#x20;  "Ó tu que estás distante do Amigo, 

&#x20;   Como um homem doente que se afastou de seu médico, 

&#x20;   Não me importunes, pois estou fora de mim; 

&#x20;   Meu entendimento se foi, não posso cantar louvores.

O que quer que diga aquele cuja razão assim se perdeu, 

Que não se vanglorie — seus esforços são inúteis. 

O que quer que diga é inoportuno, 

Seguramente inadequado e distante da verdade. 

Que posso dizer quando já nenhum de meus nervos tem sensibilidade? 

Posso eu explicar o Amigo a alguém de quem Ele não é Amigo?

----

7- SHAMSUDDIN: Mestre espiritual e companheiro de Rumi.
