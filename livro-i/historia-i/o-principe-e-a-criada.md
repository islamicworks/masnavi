# O Principe e A Criada

Um príncipe estava em uma caçada, quando avistou ao longe uma linda moça e, com promessas de ouro, induziu-a a acompanhá-lo. Passado um tempo, a moça adoeceu e o príncipe convocou vários médicos para tratá-la. Como, porém, todos deixaram de dizer "se Deus quiser, iremos curá-la", o tratamento não teve efeito. Então o príncipe fez uma prece e, em resposta, o céu enviou-lhe um médico. Logo este condenou a opinião de seus colegas acerca do caso e, por meio de um diagnóstico muito hábil, descobriu que a verdadeira causa da doença da moça era seu amor por um certo ourives de Samarcanda. Seguindo o conselho de médico, o príncipe mandou buscar o ourives em Samarcanda e casou-o com a moça doente de amor; por seis meses o par viveu em grande harmonia e felicidade. No fim desse período, o médico, por ordem divina, deu ao ourives uma bebida venenosa, que fez decair sua força e beleza, e ele perdeu o favor da moça que, então, reconciliou-se com o príncipe.&#x20;

Essa ordem divina foi exatamente igual ao comando de Deus a Abraão para matar seu filho Ismael e ao ato do anjo ao matar o servo de Moisés,(5) e está, portanto, acima da crítica dos homens.

----

5 - [Alcorão XVIII.74.](https://quran.com/18/74)
