## Laila e o Califa

O Califa disse a Laila: 'Tu és realmente aquela

Por quem Majnun perdeu a cabeça e a razão?

Tu não és mais bela do que muitas outras".

Ela respondeu: "Cala-te, tu não és Majnun!"

Se tivesses os olhos de Majnun,

Tua visão abarcaria os dois mundos.

Tu estás em teu juízo, mas Majnun tem o seu perdido.

Quando se está apaixonado, estar desperto é traição.

Quanto mais desperto está o homem, mais ele dorme (para o amor),

Sua vigília crítica é pior que um cochilo.

A vigília acorrenta nosso espírito

E faz de nossa alma uma presa para caprichos,

Preocupações com perdas e ganhos e o medo da pobreza.

A alma não guarda assim nem pureza nem dignidade nem brilho,

Nem a aspiração de ascender aos céus.

Está de fato dormindo aquele que anseia por cada capricho

E dialoga com cada fantasia.

