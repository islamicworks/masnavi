# Sono do Corpo Despertar da Alma

Toda noite Tu libertas nossos espíritos do corpo

E de seus ardis, fazendo-os puros como tabulas rasas.

Toda noite, espíritos são libertados dessa jaula,

E tornados livres, sem comandar nem ser comandados.

De noite, o prisioneiro não tem consciência de sua prisão,

De noite, o rei não tem consciência de sua majestade.

Aí, não se pensa em perdas e ganhos,

Nem se dá atenção a isso ou àquilo.

Assim é o estado do "Conhecedor", mesmo quando acordado.

Deus diz: [Alcorão XVIII.18] "Tu o julgarias desperto embora adormecido,

Dormindo para as coisas do mundo, dia e noite,

Como uma pena na mão do escritor que a guia".

Aquele que não vê a mão que escreve

Tem a ilusão de que o efeito procede do movimento da pena.

Se o "Conhecedor" revelasse a natureza desse estado,

Roubaria o vulgo de seu sono sensual.

Sua alma vagueia pelo deserto que não tem similitude;

Como seu corpo, seu espírito goza de repouso perfeito;

Liberto do desejo de comer e beber,

Como um pássaro que fugiu da gaiola e da armadilha.

Quando, entretanto, é de novo atraído para uma cilada,

Ele grita por socorro ao Todo-Poderoso.

---

