# O Rei Judeu, Seu Vizir E Os Cristãos

Um certo rei judeu costumava perseguir os cristãos, desejando exterminar sua fé. Seu vizir persuadiu-o a tentar um estratagema: que ele próprio, o vizir, fosse mutilado e expulso da corte, para que buscasse refúgio entre os cristãos e disseminasse discórdia entre eles.

A sugestão do vizir foi adotada. Ele se refugiou entre os cristãos e não encontrou dificuldade em persuadi-los de que fora tratado dessa maneira bárbara por sua adesão à fé cristã. Logo conquistou completa influência sobre eles, e foi aceito como santo mártir e mestre divino. Apenas alguns homens de discernimento adivinharam sua traição; a maioria deixou-se iludir por ele.

Os cristãos dividiam-se em doze legiões, e à frente de cada uma havia um comandante. A cada um desses comandantes, o vizir deu secretamente um volume de instruções religiosas, tendo o cuidado de fazer com que cada volume contivesse instruções diferentes e contraditórias. Um volume aconselhava o jejum, outro a caridade, outro a fé, outro obras, e assim por diante.

Depois, o vizir recolheu-se a uma caverna e recusou-se a sair para instruir seus discípulos, apesar de todas as suas súplicas. Convocou os comandantes e a cada um deu instruções secretas para que se declarasse seu sucessor, guiando-se pelas instruções contidas no volume secretamente confiado a ele, e matasse todos os outros pretendentes ao cargo apostólico. Depois de dar essas instruções, ele se matou. E cada comandante declarou-se sucessor do vizir, e os cristãos dividiram-se em muitas seitas inimigas umas das outras, exatamente como o vizir pretendera.

Mas o malicioso plano não teve êxito completo, já que um grupo de fiéis invocou o nome de "Ahmed", mencionado no Evangelho (João XIV. 26) e assim foram salvos de compartilhar da ruína dos demais.


---

