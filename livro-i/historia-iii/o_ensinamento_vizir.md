# O Ensimanto do Vizir

Miríades de cristãos dele se acercavam,

Um após outro, reuniram-se em sua rua.

Então ele lhes falava de mistérios,

Mistérios do Evangelho, de estolas, de preces.

Ele pregava com palavras eloqüentes

As palavras e os atos do Messias.

Por fora, era um pregador de deveres religiosos,

Mas por dentro, um canto enganador e o ardil de um passarinheiro.

Assim os seguidores do Profeta Jesus

Foram enganados pela impostura dessa alma demoníaca.

Em seus discursos, ele misturava muitas doutrinas secretas

Sobre devoção e sinceridade da alma.

Ele lhes ensinava a bem aparentar a devoção,

Mas a dizer de seus pecados secretos: "Que importam?"

Detalhe por detalhe, ponto por ponto, aprenderam dele A hipocrisia da alma, como rosas aprenderiam do alho. Os sofistas e todos os seus discípulos Perdem o brilho ante semelhantes prédicas e discursos.

Os cristãos lhe entregaram completamente seus corações,

Pois a fé cega do vulgo não tem discernimento.

No mais íntimo de seu peito, semearam amor por ele,

E o imaginavam Vigário de Cristo;

Sim, ele, aquele amaldiçoado e caolho Dajjál!

Salva-nos, Ó Deus! que és nosso único defensor!

Ó Deus, há centenas de ardis e iscas,

E nós ainda somos como pássaros gulosos e tolos;

A todo momento nossos pés são apanhados em novas armadilhas;

Sim, cada um de nós, seja ele falcão ou Simorg! 

Tu nos libertas a cada momento, e logo

Voamos novamente para a armadilha, ó Todo-Poderoso!

