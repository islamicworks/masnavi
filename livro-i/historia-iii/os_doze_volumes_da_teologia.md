# Os Doze Volumes da Teologia

Ele preparou um pergaminho distinto destinado a cada um,

O conteúdo de cada pergaminho de diferente teor;

As regras de cada um com diferente sentido,

Esta contraditória àquela, do começo ao fim.

Em um, a via do jejum e do ascetismo

Foi feita pilar e condição da boa devoção.

Em outro se dizia: "A abstinência não traz benefícios,

Sinceridade nessa via não é senão caridade".

Em outro se dizia: 'Teu jejum e tua caridade

Te igualam a Deus;

Salvo a fé e a total resignação à vontade de Deus,

Na alegria e na dor, todas as virtudes são ardil e impostura".

Em outro se dizia: "O necessário são ações,

A doutrina da fé sem ações é ilusão".

Em outro se dizia: "Obrigações e proibições não são

Para serem observadas, e sim para revelar nossa fraqueza,

Fazer-nos ver nossa própria incapacidade de cumpri-las,

E dessa forma reconhecer e admitir o poder de Deus".

Em outro se dizia: "Qualquer referência à tua própria fraqueza

É ingratidão para com as dádivas de Deus.

Considera antes teu poder, pois teu poder provém de Deus.

Sabe que teu poder é graça de Deus, pois pertence a Ele".

Em outro se dizia: "Esquece o poder e a fraqueza,

O que quer que afaste teus olhos de Deus é um ídolo".

Em outro se dizia: "Não apagues tua tocha terrena,

Para que ela possa ser uma luz a iluminar a humanidade.

Se negligenciares a consideração e o cuidado com ela,

Apagarás à meia-noite a lâmpada da união".

Em outro se dizia: "Apaga essa tocha sem medo,

Que em vez de uma possas ver mil alegrias,

Pois extinguindo a luz, a alma se alegra,

E tua Laila (noite) se torna tão audaz quanto seu Majnun.

Quem, para exibir sua devoção, renuncia ao mundo

O terá sempre consigo, atrás e diante de si".

Em outro se dizia: "O que quer que Deus te tenha dado

Em Sua criação, Ele o fez doce para ti;

Sim, agradável e permissível. Toma-o, pois,

E não te lances nas dores da abstinência".

Em outro se dizia: "Abandona tudo o que possuis,

Pois ser governado pela avidez é um grave pecado".

(Ah! Quantas vias diferentes são mostradas,

E cada uma seguida por alguma seita em nome da salvação!

Se a via certa fosse facilmente atingível,

Todo judeu e guebro a teria encontrado!

Em um se dizia: "A via certa é atingível

Pois o alimento da alma é a vida do coração.

O que for usufruído pelo homem carnal não gera fruto,

Como a terra do deserto e o sal.

Seu resultado não é senão remorso,

Seu comércio não gera senão prejuízo,

Não traz proveito a longo prazo,

Seu nome é ao final falência.

Discerne, então, o falido do lucrativo,

Considera o valor potencial disto e daquilo".

Em outro se dizia: "Escolhe para ti um Guia sábio,

Mas a previsão de resultados não é encontrada em celebridades".

(Cada seita previu resultados de forma diferente,

E todas se tornaram prisioneiras de erros.

A verdadeira previsão não é simples passe de mágica,

Pois, se fosse, todas essas diferenças não teriam surgido.)

Em um se dizia: 'Tu és teu próprio mestre,

Pois já travaste conhecimento com o Mestre de tudo;

Sê um homem e não a besta de carga de outro homem!

Segue teu próprio caminho e não percas tua cabeça!”

Em outro se dizia: 'Tudo o que vemos é Um.

Quem diz que é dois, sofre de visão dupla".

Em outro se dizia: "Cem são como um".

Mas quem quer que assim pense é louco.

Cada pergaminho tinha sua parte contrária de retórica,

Totalmente oposta em forma e substância;

Este contrário àquele, do primeiro ao último,

Como se cada um fosse composto de veneno e antídotos.

