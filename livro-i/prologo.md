# Prólogo

Escuta a flauta de bambu, como se queixa,

Lamentando seu desterro: 

&#x20;  "Desde que me separaram de minha raiz, 

&#x20;    Minhas notas queixosas arrancam lágrimas de homens e mulheres. 

&#x20;    Meu peito se rompe, lutando para libertar meus suspiros, 

&#x20;    E expressar os acessos de saudade de meu lugar.

&#x20;    Aquele que mora longe de sua casa 

&#x20;    Está sempre ansiando pelo dia em que há de voltar.

&#x20;    Ouve-se meu lamento por toda a gente, 

&#x20;    Em harmonia com os que se alegram e os que choram.

&#x20;    Cada um interpreta minhas notas de acordo com seus sentimentos,

&#x20;    Mas ninguém penetra os segredos de meu coração. 

&#x20;    Meus segredos não destoam de minhas notas queixosas, 

&#x20;    E, no entanto não se manifestam ao olho e ao ouvido sensual. 

&#x20;    Nenhum véu esconde o corpo da alma, nem a alma do corpo,

&#x20;    E, no entanto homem algum jamais viu uma alma” 

O lamento da flauta é fogo, e não puro ar. 

Que aquele que carece desse fogo seja tido como morto!

É o fogo do amor que inspira a flauta,(1)

E o amor que fermenta o vinho. 

A flauta é confidente dos amantes infelizes; 

Sim, sua melodia desnuda meus segredos mais íntimos. 

Quem viu veneno e antídoto como a flauta? 

Quem viu consolador gentil como a flauta? 

A flauta conta a história do caminho, manchado de sangue, do amor, 

Conta a história das penas de amor de Majnun.(2)

Ninguém sabe desses sentimentos senão aquele que está louco,\

Como um ouvido que se inclina aos sussurros da língua. \

De pena, meus dias são trabalho e dor, \

Meus dias passam de mãos dadas com a angústia. \

E, no entanto, se meus dias se esvaem assim, não importa, \

Faz tua vontade, ó Puro Incomparável! \

Mas quem não é peixe logo se cansa da água; \

E àqueles a quem falta o pão de cada dia, o dia parece muito longo;\

Assim o "Verde" não compreende o estado do "Maduro";(3)\

Portanto cabe a mim abreviar meu discurso. \

Levanta-te, ó filho! Rompe tuas cadeias e se livre! \

Quanto tempo serás cativo da prata e do ouro? Embora despejes o oceano em teu cântaro,\

Este não pode conter mais que a provisão de um dia. \

O cântaro do desejo do ávido nunca se enche, \

A ostra não se enche de pérolas até a saciedade; \

Somente aquele cuja veste foi rasgada pela violência do amor\

Está inteiramente puro, livre de avidez e de pecado. \

A ti entoamos louvores, ó Amor, doce loucura! \

Tu que curas todas as nossas enfermidades! \

Que és médico de nosso orgulho e presunção! \

Tu que és nosso Platão e nosso Galeno!(4) \

O amor eleva aos céus nossos corpos terrenos, \

E faz até os montes dançarem de alegria! \

Ó amante, foi o amor que deu vida ao Monte Sinai, \

Quando "o monte estremeceu e Moisés perdeu os sentidos".\

Se meu Amado apenas me tocasse com seus lábios, \

Também eu, como a flauta, romperia em melodias. \

Mas aquele que se aparta dos que falam sua língua, \

Ainda que tenha cem vozes, é forçosamente mudo. \

Depois que a rosa perde a cor e o jardim fenece, \

Não se ouve mais a canção do rouxinol. \

O Amado é tudo em tudo, o amante, apenas seu véu; \

Só o Amado é que vive, o amante é coisa morta. \

Quando o amante não sente mais as esporas do Amor, \

Ele é como um pássaro que perdeu as asas. \

Ai! Como posso manter os sentidos, \

Quando o Amado não mostra a luz de Seu semblante? \

O Amor quer ver seu segredo revelado, Pois se o espelho não reflete, de que servirá? \

Sabes por que teu espelho não reflete? \

Porque a ferrugem não foi retirada de sua face. \

Fosse ele purificado de toda ferrugem e mácula, \

Refletiria o brilho do Sol de Deus. \

Ó amigos, ouvi agora esta narrativa, \

Que expõe a própria essência de minha situação.

----

1- Amor, aqui, é a forte atração que move todas as criaturas a se reunirem a seu Criador.&#x20;

2- MAJNUN: o célebre "louco" de amor da literatura persa e árabe. Impedido de ver sua amada Laila, ele abandona as riquezas e o mundo para vagar sozinho no deserto, entre as feras.

3-  "Verde" e "Maduro" são termos para "Homens de externalidades" e '"Homens de Coração” ou Místicos.&#x20;

4-  GALENO: um dos pilares da mediana, cuja doutrina foi difundida pelos árabes na Idade Média.
