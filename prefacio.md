# 💡 Prefácio

Rumi nasceu em Khulm e, logo após seu nascimento, sua família mudou-se para Balkh, a 10 km de distância. Seu pai era Naqshband e encadernador por profissão. Rumi foi educado em ambas as coisas desde sua infância.

A razão de sua viagem a Gazorgah era que ali havia uma imensa biblioteca e seu tio fora contratado para reencadernar os livros. Rumi ali viveu por nove anos. Depois foi a Qandahar para estudar Tafsil, isto é, a "compreensão do Alcorão", na mesquita onde está o manto do Profeta.

Naquele tempo, esta região ainda pertencia ao Khorassan, que se estendia por Herat, Hazarayat, Mashad, até o mar Cáspio. O Sheik Naqshband de toda essa região era o Sayed Zahir Shah, de quem Rumi tornou-se discípulo.

Zahir Shah ordenou que Rumi fizesse uma viagem de sete anos sem lhe dizer aonde deveria ir. O dia de seu regresso coincidiu com o funeral de seu Sheik. Pediram-lhe que fosse à casa de Zahir, dizendo-lhe que este lhe havia deixado algumas coisas. Estas eram um tasbih (rosário) e um manto - o manto de um Sheik Naqshband. Assim, Rumi tornou-se o sucessor de Zahir Shah, e permaneceu em Gazorgah, Herat e Mazar-i-Sharif pelos sete anos seguintes.

Ao final do Ramadan, Rumi regressou a Qandahar, onde, na Grande Mesquita, se encontra o manto do Profeta, guardado em uma caixa na parede, que não pode ser aberta senão na presença de quatro Sayeds. Naquele ano, quatro Sayeds estavam em Qandahar e decidiram abrir a caixa e tirar o Manto, e todas as pessoas reuniram-se na Mesquita para vê-lo. Rumi, que estava na Mesquita com as demais pessoas, dirigiu-se a alguém a seu lado, a quem não conhecia, e disse: "Amigo, podes levar-me a casa onde estou hospedado, pois fiquei cego?" — "Não," disse o homem que se vestia todo de verde, "mas vem comigo e iremos a Konya."

Dessa maneira, eles partiram, e, quando chegaram a Konya, o Homem Verde lhe disse: "Agora podes ver, e te proporcionei ainda a habilidade de ver qual é tua missão aqui", e desapareceu. Rumi então conseguiu uma pequena cabana, onde hoje é a cidade nova de Konya, e começou a falar às pessoas e a trabalhar e a dar conferências. Um ano depois, em vinte e um do Ramadan, Rumi dormia, quando foi despertado por alguém que o sacudia. Percebeu que o Homem Verde estava a seu lado, dizendo-lhe: "Jalaluddin, por que não estás escrevendo?" Rumi respondeu: "Khidr Elias, eu não sei escrever". Ao que o Homem Verde disse: "Não tenho tempo para ensinar-te; assim, escreve!" Rumi então se levantou e viu que diante dele havia papel, pena e tinta, e começou a escrever. E escreveu o Masnavi.

Algum tempo depois, porque as coisas que Rumi estava dizendo e fazendo se tornaram "muito fortes", apareceu em Konya um homem chamado Shams. Ele veio para que aquelas palavras e ações de Rumi que fossem demasiado poderosas pudessem ser absorvidas e diluídas através dele, tornando-se assim compreensíveis para as pessoas. Depois de alguns anos, Shams regressou a Bagdá, onde morreu e está sepultado. Sua tumba está à esquerda da de Al-Ghazzali, no mausoléu de Al-Ghazzali, e, embora a inscrição esteja ilegível, ainda pode ser identificada pelo número 613. De acordo com o sistema abjad, (ABJAD: Sistema alfanumérico da língua árabe, equivalente à Cabala hebraica.) 613 equivale alfabeticamente a Shams. Por ser conhecido como Shams-i-Tabriz, acredita-se que ele seja proveniente de Tabriz, uma cidade da Pérsia. Porém, seu verdadeiro nome era Sayed Shamsuddin Shah, e era filho do Sayed Zahir Shah.

Rumi está sepultado exatamente no lugar que ele mesmo indicou, em Konya. Ali existem quatro criptas, e sua tumba está alguns metros abaixo da superfície.

Certos detalhes da vida de Rumi eram irrelevantes, mas certos pontos eram muito importantes. Tais pontos eram aqueles para onde fora enviado por seu Sheik. Sua vida se divide em períodos de diferentes durações, e cada movimento significava que se havia cumprido uma certa etapa e ele passava à seguinte. O próprio Rumi pode não ter sabido aonde ia e por quanto tempo, mas seu Sheik sim o sabia, pois fora ele que planejara a configuração total desde o princípio. O único fator desconhecido era a duração exata de cada etapa.

Pode acontecer que, em um momento do caminho, um Sheik diga a seu discípulo: "Agora volta para tua casa". Isto será muito difícil para esse homem, porém esta ordem é definitiva e não há possibilidade mediante a qual esse homem possa continuar.

O caminho, em sua totalidade, é muito difícil. Attar dizia que por vezes parecia-lhe que todos e cada um de seus nervos se crispavam e afloravam à superfície de seu corpo, de maneira que uma brisa, por mais suave que fosse, sobre sua pele, causava-lhe um enorme sofrimento. No entanto, apesar das dificuldades e dos sofrimentos, estima-se que apenas 4% são descartados, e este cálculo é feito através dos séculos, não somente dos anos. Esta cifra tão baixa deve-se ao fato de que as pessoas não são tomadas ao acaso, e sim escolhidas, e o Sheik conhece a quem está escolhendo.

Rumi criou a dança Mevlevi, que ele próprio designava uma criação estratégica. Entre Europa e Turquia havia uma necessidade de ajustamento, alinhamento e controle. A dança Mevlevi, o sarna, era este controle.

(Extrato de uma transcrição de duas falas de Omar Ali Shah, em Konya, 1982)
