# O Príncipe Que, Depois De Ter Sido Enganado Por Uma Cortesã, Voltou ao Seu Verdadeiro Amor

Um certo rei sonhou que seu muito querido filho, um jovem promissor, tivera um fim prematuro. Ao acordar, ficou feliz de descobrir que seu filho ainda estava vivo; mas refletiu que um acidente poderia levá-lo a qualquer momento e, portanto, resolveu casá-lo sem demora, a fim de que a sucessão fosse garantida. Conseqüentemente, escolheu a filha de um piedoso dervixe para noiva de seu filho e fez os preparativos para o casamento. Sua mulher, porém, e as outras mulheres do seu harém não aprovaram essa união, considerando abaixo da dignidade do príncipe casar-se com a filha de um mendigo. O rei repreendeu-as dizendo que um dervixe que renunciara às riquezas do mundo por amor a Deus não deveria ser confundido com um mendigo comum e insistiu na consumação do casamento. Depois do casamento, o príncipe recusou-se a manter relações com a noiva, embora ela fosse muito bonita, e continuou uma relação com uma velha feia que o enfeitiçara por meio de bruxaria. Passado um ano, porém, o rei descobriu médicos que conseguiram quebrar o feitiço, e o príncipe recuperou a razão e seus olhos se abriram para os encantos superiores de sua mulher, e ele abandonou sua feia amante e apaixonou-se por sua mulher.

Trata-se de uma parábola, onde a mulher verdadeira é a Divindade, a amante velha, o mundo, e os médicos, os profetas e santos.

Outra ilustração é uma criança que brincava de sitiar um forte de mentira com seus amiguinhos, e conseguiu capturá-lo, deixando os outros de fora. Nesse momento, Deus "outorgou-lhe a sabedoria, embora ainda criança", (234) e chegou a ele o dia em que "o homem foge de seu irmão", (235) e ele reconheceu o vazio desse jogo ocioso e passou a dedicar-se à busca da santidade e da piedade.

\----

* 234 [Alcorão XIX. 12](htts://quran.com/12/).
* 235 - [Alcorão LXXX. 34](https://quran.com/34/).
