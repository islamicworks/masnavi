# 🎥 Apresentação

É com muito prazer que apresento o Masnavi de Maulana Jalaluddin Rumi Este livro extraordinário, que tem sido chamado "o Alcorão em persa”, é um dos mais importantes textos fundadores da Tradição Sufi.&#x20;

O estudo deste livro revela a altura que o ser humano pode alcançar em sua busca por um lugar no Grande Desenho do Universo. A cada leitura, camadas mais e mais profundas de significação tornam-se manifestas, e mais o buscador é encorajado a desenvolver níveis de compreensão ainda mais profundos.&#x20;

Recomendo o estudo deste livro para todos aqueles cujos sentimentos os dirigem para o caminho do desenvolvimento moral e espiritual. Deve ser lido com atenção e disciplina.&#x20;

_Sayed Omar Naqshband_
