# Table of contents

* [💚 Masnavi](README.md)
* [🎥 Apresentação](apresentacao.md)
* [💡 Prefácio](prefacio.md)
* [💌 Nota dos Editores](nota-dos-editores.md)
* [📖 Glossário](GLOSSARY.md)

## 📗 Livro I

* [🎼 Prólogo](livro-i/prologo.md)
* [🗝 História I](livro-i/historia-i/README.md)
  * [O Principe e A Criada](livro-i/historia-i/o-principe-e-a-criada.md)
  * [Descrição do Amor](livro-i/historia-i/descricao-do-amor.md)
  * [Shamsudin De Tabriz Assedia Jalaluddin Para Que Componha o Masnavi](livro-i/historia-i/shamsudin-de-tabriz-assedia-jalaluddin-para-que-componha-o-masnavi.md)
* [🗝 História II](livro-i/historia-ii/README.md)
  * [O Azeiteiro e o Seu Papagaio](livro-i/historia-ii/o-azeiteiro-e-seu-papagaio.md)
  * [Confusão de Santos Com Hipocritas](livro-i/historia-ii/confusao-de-santos-com-hipocritas.md)
* [🗝 História III](livro-i/historia-iii/README.md)
  * [O Rei Judeu, Seu Vizir E Os Cristãos](livro-i/historia-iii/o_rei_judeu,_seu_vizir_e_os_cristaos.md)
  * [O Ensinamento do Vizir](livro-i/historia-iii/o_ensinamento_vizir.md)
  * [O Sono do Corpo, Despertar da alma](livro-i/historia-iii/o_sono_do_corpo_despertar_da_alma.md)
  * [Laila e o Califa](livro-i/historia-iii/laila_e_o_califa.md)
  * [Os Doze Volumes da Teologia](livro-i/historia-iii/os_doze_volumes_da_teologia.md)
* [🗝 História IV](livro-i/historia-iv.md)
* [🗝 História V](livro-i/historia-v.md)
* [🗝 História VI](livro-i/historia-vi/README.md)
  * [Moisés e Faraó](livro-i/historia-vi/moises-e-farao.md)
  * [Sobre A Tradição](livro-i/historia-vi/sobre-a-tradicao.md)

## 📗 Livro II

* [Page 1](livro-ii/page-1.md)

## 📗 Livo III <a href="#livro-iii" id="livro-iii"></a>

* [🗝 História I](livro-iii/historia-i/README.md)
  * [📖 OS VIAJANTES QUE COMERAM O FILHOTE DE ELEFANTE](livro-iii/historia-i/os-viajantes-que-comeram-o-filhote-de-elefante.md)
  * [📖 OS CUIDADOS DE DEUS PARA COM SEUS FILHOTES](livro-iii/historia-i/os-cuidados-de-deus-para-com-seus-filhotes.md)
  * [📖 AS MÁS AÇÕES DÃO ÀS PRECES DOS HOMENS MAU CHEIRO JUNTO ÀS NARINAS DE DEUS](livro-iii/historia-i/as-mas-acoes-dao-as-preces-dos-homens-mau-cheiro-junto-as-narinas-de-deus.md)

## 📗 Livro IV

* [🗝 História VIII](livro-iv/historia-viii/README.md)
  * [O Príncipe Que, Depois De Ter Sido Enganado Por Uma Cortesã, Voltou ao Seu Verdadeiro Amor](livro-iv/historia-viii/o-principe-que-depois-de-ter-sido-enganado-por-uma-cortesa-voltou-ao-seu-verdadeiro-amor.md)

## 📗 Livro V

* [História I](livro-v/historia-i.md)
* [História II](livro-v/historia-ii.md)
* [História III](livro-v/historia-iii/README.md)
  * [O SÁBIO E O PAVÃO](livro-v/historia-iii/o-sabio-e-o-pavao.md)
  * [NÃO HÁ MONGES NO ISLAM](livro-v/historia-iii/nao-ha-monges-no-islam.md)
  * [PRECES A DEUS PARA QUE TRANSFORME NOSSAS BAIXAI INCLINAÇÕES E NOS DÊ ASPIRAÇÕES MAIS ALTAS](livro-v/historia-iii/preces-a-deus-para-que-transforme-nossas-baixai-inclinacoes-e-nos-de-aspiracoes-mais-altas.md)

