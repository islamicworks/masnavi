# 📖 AS MÁS AÇÕES DÃO ÀS PRECES DOS HOMENS MAU CHEIRO JUNTO ÀS NARINAS DE DEUS

Tu estás dormindo, e o cheiro daquele fruto proibido

Eleva-se ao céu azul;&#x20;

— Eleva-se junto com teu mau hálito,

&#x20;   Até que inunda o céu com seu fedor; —&#x20;

Fedor de orgulho, fedor de luxúria, fedor de voracidade.&#x20;

Todos esses fedem como cebolas quando, um homem fala.

Embora tu jures, dizendo:&#x20;

"Quando comi? Não me abstive de cebola e alho?"

O próprio hálito desse juramento conta histórias,

Ao atingir as narinas de quem se senta contigo.

Assim também, as preces são invalidadas por esses fedores,(127)

&#x20;Esse coração torto é traído por sua fala.&#x20;

A resposta àquela prece é: "Sede conduzidos ao inferno",(128)&#x20;

A vara da repulsa é a recompensa de todo logro.&#x20;

Mas, se tua fala é torta e teu sentido reto,&#x20;

A tortuosidade de tuas palavras será aceita por Deus.

Aquele fiel Bilal, quando chamava para a oração,&#x20;

Gritava devotamente: "Aproximai-vos, aproximai-vos!"&#x20;

Por fim os homens disseram: "Ó Profeta, esse chamado não está certo,&#x20;

Está errado; ora, qual é tua intenção?&#x20;

Ó Profeta e embaixador do Todo-Poderoso!&#x20;

Providencia outro Muezin (129) com mais talento,&#x20;

É um erro no começo de nosso serviço divino&#x20;

Pronunciar as palavras 'vinde ao refúgio'!"(130)&#x20;

A ira do Profeta ferveu, e ele disse&#x20;

(Revelando um ou dois segredos da fonte da graça)&#x20;

"Ó homens vis, aos olhos de Deus,&#x20;

o 'Hu’131 de Bilal&#x20;

É melhor que cem 'vinde' e preces fervorosas.&#x20;

Ah!, não provoqueis um tumulto, para que eu não venha a dizer abertamente&#x20;

Vossos pensamentos secretos, do primeiro ao último.&#x20;

Se não mantendes vosso hálito doce na oração,&#x20;

Ide, pedi uma prece da Irmandade da&#x20;

Pureza!" Por essa razão, falou Deus a Moisés

Quando ele pedia ajuda em oração:

"O Moisés! Pede de Mim proteção

> Com uma boca com a qual não tenhas pecado".
>
> Moisés respondeu: 'Tai boca não possuo".&#x20;

Deus disse: "Invoca-me com outra boca!&#x20;

Age de modo que todas as tuas bocas,&#x20;

Noite e dia, estejam elevando orações.&#x20;

Quando tiveres pecado com uma boca,

Com tua outra boca clama: 'Ó Allah!'

Ou então limpa tua própria boca,&#x20;

E deixa teu espírito alerta e desperto.

Invocar a Deus é puro, e quando a pureza se aproxima,&#x20;

A impureza se levanta e vai embora.&#x20;

Os contrários fogem dos contrários;&#x20;

Quando o dia amanhece, a noite foge.&#x20;

Quando o nome puro (de Deus) entra na boca,&#x20;

Nem a impureza nem essa boca impura permanecem!

----

127 - O Profeta proibiu aos crentes que comam alho e cebola crus antes de irem à mesquita.&#x20;

128 - Alcorão XXIII. 108: "Deus responderá: 'Sede conduzidos a ele e não me dirijais a palavra".&#x20;

129 - MUEZIN: O fiel cuja função é chamar, do alto do minarete da mesquita, os homens para a prece islâmica.&#x20;

130 - As regras para o chamado à oração são dadas em Mishkat ul Masabih, 1.141.

131 - HU: Um nome de Deus; significa /Ele/.&#x20;
