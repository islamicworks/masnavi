# 📖 OS CUIDADOS DE DEUS PARA COM SEUS FILHOTES

Ó filho, os piedosos são filhos de Deus;&#x20;

Ausente ou presente, Ele conhece seu estado.&#x20;

Não penses que Ele está ausente quando estão em perigo,&#x20;

Pois Ele zela por suas vidas. Ele disse: "Esses santos são Meus filhos,

Embora distantes e sozinhos e apartados de seu Senhor.&#x20;

Por causa de sua provação, são órfãos e desgraçados,&#x20;

Mas, no amor, estou sempre em comunhão com eles.&#x20;

Tu tens o apoio de toda a Minha proteção,

Meus filhos são como partes de Mim.&#x20;

Em verdade, esses Meus dervixes&#x20;

São milhares e mais milhares,

e no entanto, não são mais que Um;&#x20;

Pois, se assim não fosse, como teria podido Moisés com uma vara mágica&#x20;

Virar de pernas para o ar o reino do Faraó?

E, se assim não fosse, como teria podido Noé, com uma só maldição,

Fazer igualmente Leste e Oeste submergir em seu dilúvio?&#x20;

Nem teria podido uma prece do eloqüente Lot

Arrasar contra a vontade deles a cidade fortificada;&#x20;

Essa poderosa cidade, semelhante ao Paraíso,&#x20;

Converteu-se num Tigre de águas negras; vai, vê seus vestígios!

Rumo a Síria estão estes vestígios e monumentos,

Podes vê-los viajando a caminho de Jerusalém.

Milhares de profetas tementes a Deus,&#x20;

Em todas as épocas, tinham em seu poder castigos divinos.&#x20;

Se eu falasse deles, meus limites seriam excedidos,&#x20;

E não os corações apenas, mas os próprios montes sangrariam".
