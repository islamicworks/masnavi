# 📖 OS VIAJANTES QUE COMERAM O FILHOTE DE ELEFANTE

Um grupo de viajantes perdeu-se no deserto e estavam a morrer de fome. Enquanto pensavam o que fazer, aproximou-se deles um Sábio e teve pena de sua infeliz situação. Disse-lhes que havia muitos filhotes de elefante no bosque próximo, um dos quais lhes forneceria uma excelente refeição, mas, ao mesmo tempo, alertou-os de que, se matassem um, certamente os pais os perseguiriam e se vingariam deles por matarem seu filhote.

Pouco depois, os viajantes viram um filhote rechonchudo de elefante, e não puderam resistir a matá-lo e comê-lo. Só um se conteve. Enfim, deitaram-se para descansar; tão logo adormeceram, porém, um elefante enorme apareceu e começou a farejar o hálito década um dos adormecidos. A todos os que exalavam o cheiro da carne do filhote, matou sem piedade, poupando apenas aquele que fora prudente o bastante para se abster.
