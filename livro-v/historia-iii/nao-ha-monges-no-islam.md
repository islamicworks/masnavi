# NÃO HÁ MONGES NO ISLAM

(260)

Não arranques tua plumagem, isto não pode ser reposta;&#x20;

Não desfigures tua face em desespero, ó formoso!&#x20;

Esse rosto que brilha como o sol da manhã,&#x20;

Desfigurá-lo seria grave pecado.&#x20;

Seria ímpio desfigurar uma face como a tua!&#x20;

A própria lua choraria a perda de tal visão!&#x20;

Não conheces a beleza de tua própria face?&#x20;

Abandona esse impulso que te leva a lutar contigo mesmo!&#x20;

São as garras de teus próprios pensamentos tolos&#x20;

Que de despeito ferem o rosto de tua alma tranqüila.&#x20;

Sabe que tais pensamentos são garras carregadas de veneno,&#x20;

Que sulcam fundas feridas na face de tua alma.&#x20;

Não dilaceres tua plumagem, mas a desvia dela teu coração,

Pois a oposição é uma condição necessária para essa guerra santa.&#x20;

Não houvesse hostilidade, essa guerra seria impossível.&#x20;

Não tivesses luxúria, não poderia haver obediência à lei.&#x20;

Não tivesses concupiscência, não poderia haver abstinência.&#x20;

Onde não existe antagonista, que necessidade há de exércitos?&#x20;

Ah! Não te faças eunuco, não te convertas em monge,&#x20;

Porque a castidade está hipotecada à luxúria.&#x20;

Sem luxúria, a negação da luxúria é impossível;

&#x20;Nenhum homem pode exibir bravura contra os mortos.&#x20;

Deus diz: "Gasta",(26)1 por isso, ganha dinheiro .&#x20;

Não é o gasto impossível sem o ganho prévio?&#x20;

Embora a passagem contenha apenas a palavra "gasta",

Le: "Primeiro ganha, depois gasta".&#x20;

Da mesma forma, quando o Rei dos reis diz: "Abstém-te",(262)&#x20;

Isso implica em um objeto de desejo do qual se abster.&#x20;

Da mesma forma, diz-se "comei" reconhecendo os ardis da luxúria,&#x20;

E depois, "não vos excedais" para impor a moderação."(263)

Quando não há sujeito,&#x20;

A existência de um predicado não é possível.&#x20;

Quando não suportas as dores da abstinência,&#x20;

E não satisfazes as condições, não ganhas recompensa alguma.&#x20;

Como são fáceis essas condições! Como é abundante essa recompensa! Uma recompensa que maravilha o coração e inebria a alma!&#x20;

Segue-se uma admoestação de que o único caminho para ficar a salvo dos próprios inimigos internos é aniquilar o eu, e deixar-se absorver na eternidade de Deus, assim como a luz das estrelas se perde na luz do sol do meio-dia. Tudo, exceto Deus, é logo presa de outros, e por sua vez aprisiona a outros, como a ave que, ao pegar uma minhoca, foi por sua vez apanhada por um gato. Os homens estão tão ocupados com seus próprios objetivos sem valor, que não vêem seus inimigos que estão tentando fazer deles a sua presa. Assim é dito: "Ante eles colocamos uma barreira e atrás outra, para que não possam ver".(264) As pessoas que anseiam pelos prazeres vis deste mundo e desejam uma longa vida, não para servir a Deus, mas para satisfazer seus próprios desejos carnais, assemelham-se ao corvo sacrificado por Abraão, porque ele vivia pelo prazer da carniça; ou Iblis, que rezou para ser poupado até o Dia do Juízo, não com o objetivo de reformar-se, mas apenas para fazer o mal à humanidade.24 (265)

----

260 - Um hadith.

261 - [Alcorão II:264](https://quran.com/2/264)

262 - [Alcorão III.200](https://quran.com/3/200)

263 - [Alcorão VI.141](https://quran.com/6/141): "Comei de seus frutos quando frutificarem e pagai o tributo no dia da colheita. E não vos excedais".

264 - [Alcorão XXXVI.9](https://quran.com/36/9)

265 - [Alcorão VII.14](https://quran.com/7/14)
