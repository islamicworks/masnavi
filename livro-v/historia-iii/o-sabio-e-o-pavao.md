# O SÁBIO E O PAVÃO

Um sábio saiu para arar seu campo e viu um pavão ocupado em destruir a própria plumagem com seu bico. Ao ver essa autodestruição insana, o sábio não pôde conter-se e gritou ao pavão e parasse de se mutilar desperdiçando sua beleza de maneira tão brutal. O pavão então explicou-lhe que a bela plumagem que ele tanto admirava era fonte de perigo para seu desafortunado dono, já que fazia com que fosse constantemente perseguido por caçadores, contra quem ele não tinha forças para lutar; devido a isso, ele decidira livrar-se dela com seu próprio bico, tornando-se tão feio que nenhum caçador iria preocupar-se em molestá-lo no futuro.

O poeta prossegue assinalando que a inteligência, o talento e as riquezas mundanas põem em perigo a vida espiritual do homem, como a plumagem do pavão; não obstante, porém, nos são dadas para pôr-nos à prova e sem tais provas não haveria virtude.

