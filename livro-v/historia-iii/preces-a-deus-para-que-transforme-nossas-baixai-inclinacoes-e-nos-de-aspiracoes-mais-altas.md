# PRECES A DEUS PARA QUE TRANSFORME NOSSAS BAIXAI INCLINAÇÕES E NOS DÊ ASPIRAÇÕES MAIS ALTAS

Ó Tu que convertes a terra em ouro,&#x20;

E de outra terra fizeste o pai da humanidade,&#x20;

Teu ofício é transformar e conceder favores,&#x20;

Meu ofício são os erros, o esquecimento e os enganos.&#x20;

Converte meus erros e esquecimentos em conhecimento;&#x20;

Sou inteiramente desprezível, faz-me equilibrado e humilde.&#x20;

O Tu que convertes a terra salgada em pão,&#x20;

E o pão na vida dos homens;&#x20;

Tu que fizeste da alma errante um guia para os homens,&#x20;

E daquele que se desviou do caminho um profeta;\~D(266)&#x20;

Tu fazes de alguns homens nascidos na terra um Céu,&#x20;

E multiplicas os santos nascidos no Céu sobre a terra!&#x20;

Mas àquele que busca sua água da vida nas alegrias do mundo,

Vem a morte mais depressa que aos outros.&#x20;

Os olhos do coração que contemplam os céus&#x20;

Vêem o Alquimista Todo-Poderoso sempre trabalhando aqui.&#x20;

A humanidade está sempre sendo transformada,&#x20;

E o elixir de Deus costura as vestes do corpo sem ajuda de agulha.&#x20;

No dia em que entraste na existência,&#x20;

Foste primeiro fogo, ou terra, ou ar.&#x20;

Se tivesses continuado nesse teu estado original,&#x20;

Como poderias ter chegado a essa dignidade de ser humano?&#x20;

Mas, pela transformação, tua primeira existência não persistiu;&#x20;

Em lugar dela, Deus te deu uma existência melhor.&#x20;

Da mesma maneira que te dará milhares de existências,&#x20;

Uma após outra, as seguintes melhores que as primeiras.&#x20;

Considera teu estado original, não os estados intermediários,&#x20;

Pois esses estados te afastam de tua origem.&#x20;

À medida que crescem esses estados intermediários, a união&#x20;

\[retrocede; À medida que decrescem, a unção da união aumenta.&#x20;

Pelo conhecimento dos meios e das causas o santo aturdimento&#x20;

\[fracassa; Sim, o aturdimento que te leva à presença de Deus. Obtiveste essas existências depois de aniquilações; Por que, então, temes a aniquilação? Que mal essas aniquilações te fizeram Para que te agarres a tua existência presente, ó tolo? Já que os últimos de teus estados eram melhores que os primeiros, Busca a aniquilação e estima a mudança de estado. Já viste centenas de ressurreições Ocorrerem acada momento, de tua origem até agora; Uma do estado inorgânico para o estado vegetativo, Do estado vegetativo para o estado animal das provações; Depois novamente para a racionalidade e o bom discernimento; Outra vez te erguerás desse mundo dos sentidos e das formas.&#x20;

Ah!, Ó corvo, abandona essa vida e vive de novo! Almejando as mudanças de Deus, lança longe tua vida! Escolhe o novo, desiste do velho, Pois cada ano presente é melhor que três passados.&#x20;

Segue-se um comentário sobre o hadith do Profeta: "Compadece-te do devoto que cai em pecado, do homem rico que cai na pobreza e do homem sábio que cai na companhia de tolos". Isso ' ilustrado por uma anedota de um jovem cervo que foi posto no estábulo dos asnos, que zombaram dele e o maltrataram.

----

266 - [Alcorão XCIII.7](https://quran.com/43/7).
