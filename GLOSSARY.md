# Glossário

## Masnavi de Maulana Jalaluddin Rumi

## Maulana 'alalu·'d-din Muhammad Rumi

## Maulana Jalaluddin Rumi

## Dajjál

O Anticristo.

##  Simorg 

Pássaro mítico (rei dos pássaros) da tradição persa.

## João XIV. 26

João, capitulo 14, verso 26: "Mas o Espírito Santo (parakletos) vos ensinará todas as coisas". 

Parakletos significa em grego 'louvado", que é também o significado do nome "Mohammed", cuja raiz é a mesma que a do outro nome do Profeta, "Ahmed

## Alcorão XVIII.18

Diz-se dos Sete Adormecidos na caverna.

Acorão Sura 18, Al-Kahf, verso 18: (Se os houvesses visto), terias acreditado que estavam despertos, apesar de estarem dormindo, pois Nós os virávamos, ora para a direita, ora para a esquerda, enquanto o seu cão dormia, com as patas estendidas, na entrada da caverna. Sim, seos tivesses visto, terias retrocedido e fugido, transido de espanto!

