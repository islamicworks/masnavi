# 💌 Nota dos Editores

O Masnavi, obra magna de Rumi, em sua essência, sugere um plano bem organizado que o próprio Rumi indiretamente apresenta nos primeiros dezoito versos de seu livro.

As várias interpretações do Masnavi sempre tenderam a privilegiar um ou mais temas sem, no entanto, referirem-se a um plano definido e organizado.

Em seu Masnavi, Rumi está, na verdade, descrevendo e discutindo os principais estágios da vida espiritual. Seu objetivo é ensinar o caminho do Amor Divino, que conduz o homem de um estágio mais baixo para um mais elevado.

A compreensão do Masnavi, a partir deste plano organizado, de acordo com os dezoito primeiros versos, cria uma ordem que fornece uma seqüência de desenvolvimento espiritual composta de nove passos:

1. O estado do homem imaturo (ou cru, ou materialista);
2. O despertar;
3. Os desejos e a busca;
4. O aprimoramento da alma através da indiferença para com as riquezas do mundo;
5. O amor;
6. A devoção e a submissão;
7. A perplexidade e a necessidade de um guia espiritual; 8) A observação de Deus em cada fenômeno; 9) A unificação,

O Masnavi está baseado em histórias morais, idéias filosóficas e espirituais. As histórias são de compreensão rápida. As idéias filosóficas e espirituais, que no Tassawuf (Misticismo Islâmico) são chamadas as pérolas que caem da boca de um mestre espiritual e formam a essência de sua fala, estão interconectadas em torno dos temas centrais dos vários estágios espirituais, ou seja, são como desdobramentos de cada estágio.

Todos os estágios estão, por sua vez, ligados ao tema maior e central que é o de levar o leitor para mais perto de Deus. Foi por causa desta qualidade que, após a morte de Rumi, em 1273, o Masnavi tornou-se tão popular na índia, Paquistão, Afeganistão, Ásia Central e Turquia, espalhando-se depois por todo o mundo.

